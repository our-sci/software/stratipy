import pytest
from stratipy import stratify, presets, constants


def test_get_sampling_density_carbofarm():
    assert stratify.get_sampling_density(3, presets.carbofarm_sampling_densities) == 2
    assert stratify.get_sampling_density(7, presets.carbofarm_sampling_densities) == 1
    assert stratify.get_sampling_density(5, presets.carbofarm_sampling_densities) == 2
    assert stratify.get_sampling_density(16, presets.carbofarm_sampling_densities) == 1
    with pytest.raises(ValueError):
        stratify.get_sampling_density(0, presets.carbofarm_sampling_densities)


def test_get_sampling_density_ESMC():
    assert (
        stratify.get_sampling_density(3, presets.esmc_sampling_densities)
        == (1 / 4) / constants.HECTARE_PER_ACRE
    )
    assert (
        stratify.get_sampling_density(100000, presets.esmc_sampling_densities)
        == (1 / 4) / constants.HECTARE_PER_ACRE
    )


def test_get_number_of_samples_sk():
    assert stratify.get_number_of_samples(2, presets.sk_sampling_density_low_area) == 10
    assert stratify.get_number_of_samples(3, presets.sk_sampling_density_low_area) == 10
    assert stratify.get_number_of_samples(7, presets.sk_sampling_density_high_area) == 3
    assert (
        stratify.get_number_of_samples(0.5, presets.sk_sampling_density_high_area) == 1
    )
    with pytest.raises(ValueError):
        stratify.get_number_of_samples(4, presets.sk_sampling_density_low_area)


def test_get_number_of_samples_carbofarm_without_min_sample_number():
    assert stratify.get_number_of_samples(1, presets.carbofarm_sampling_densities) == 2


def test_get_number_of_samples_within_range():
    densities = [
        {
            "min_area": 0,
            "max_area": 10,
            "density": 0.1,
            "min_samplings": 1,
            "max_samplings": 5,
        },
        {
            "min_area": 10,
            "max_area": 20,
            "density": 0.2,
            "min_samplings": 2,
            "max_samplings": 10,
        },
    ]
    assert stratify.get_number_of_samples(5, densities) == 1
    assert stratify.get_number_of_samples(15, densities) == 3


def test_get_number_of_samples_clamped_to_min():
    densities = [
        {
            "min_area": 0,
            "max_area": 10,
            "density": 0.05,
            "min_samplings": 2,
            "max_samplings": 5,
        }
    ]
    assert stratify.get_number_of_samples(5, densities) == 2


def test_get_number_of_samples_clamped_to_max():
    densities = [
        {
            "min_area": 0,
            "max_area": 10,
            "density": 1.0,
            "min_samplings": 1,
            "max_samplings": 5,
        }
    ]
    assert stratify.get_number_of_samples(10, densities) == 5


def test_get_number_of_samples_no_max_samplings():
    densities = [{"min_area": 0, "max_area": 10, "density": 0.5, "min_samplings": 1}]
    assert stratify.get_number_of_samples(6, densities) == 3


def test_get_number_of_samples_invalid_area():
    densities = [
        {
            "min_area": 0,
            "max_area": 10,
            "density": 0.1,
            "min_samplings": 1,
            "max_samplings": 5,
        }
    ]
    with pytest.raises(ValueError, match="invalid stratum area"):
        stratify.get_number_of_samples(15, densities)


def test_get_number_of_samples_edge_case():
    densities = [
        {
            "min_area": 0,
            "max_area": 10,
            "density": 0.1,
            "min_samplings": 1,
            "max_samplings": 5,
        }
    ]
    assert stratify.get_number_of_samples(10, densities) == 1
