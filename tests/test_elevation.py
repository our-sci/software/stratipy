import pytest
import shapely
from stratipy import elevation


def test_bbox_product_range_returns_cartesian_product_for_coordinates():
    bbox1 = (89.1, 40.1, 92, 42.9)
    actual_bbox_product_1 = elevation.bbox_product_range(bbox1)
    expected_bbox_product_1 = [
        (89, 40),
        (89, 41),
        (89, 42),
        (90, 40),
        (90, 41),
        (90, 42),
        (91, 40),
        (91, 41),
        (91, 42),
        (92, 40),
        (92, 41),
        (92, 42),
    ]
    assert actual_bbox_product_1 == expected_bbox_product_1


def test_get_3dep_tile_urls_within_single_tile():
    bbox_1 = (-89.9, 40.1, -89.1, 40.9)
    actual_1 = elevation.get_3dep_tile_urls(bbox_1, url_base="")
    expected_1 = ["n41w090/USGS_13_n41w090.tif"]
    assert actual_1 == expected_1


def test_get_3dep_tile_urls_across_two_longitude_tiles():
    bbox_2 = (-90.1, 40.1, -89.1, 40.9)
    actual_2 = elevation.get_3dep_tile_urls(bbox_2, url_base="")
    expected_2 = ["n41w091/USGS_13_n41w091.tif", "n41w090/USGS_13_n41w090.tif"]
    assert actual_2 == expected_2


def test_get_3dep_tile_urls_across_two_latitude_tiles():
    bbox_3 = (-89.9, 39.9, -89.1, 40.9)
    actual_3 = elevation.get_3dep_tile_urls(bbox_3, url_base="")
    expected_3 = ["n40w090/USGS_13_n40w090.tif", "n41w090/USGS_13_n41w090.tif"]
    assert actual_3 == expected_3


def test_get_3dep_tile_urls_across_two_longitude_and_latitude_tiles():
    bbox_4 = (-90.1, 39.9, -89.1, 40.9)
    actual_4 = elevation.get_3dep_tile_urls(bbox_4, url_base="")
    expected_4 = [
        "n40w091/USGS_13_n40w091.tif",
        "n41w091/USGS_13_n41w091.tif",
        "n40w090/USGS_13_n40w090.tif",
        "n41w090/USGS_13_n41w090.tif",
    ]
    assert actual_4 == expected_4


def test_get_copernicus_tile_url_single_tile_in_NW_hemisphere():
    bbox_NW = (-89.9, 40.1, -89.1, 40.9)
    actual_NW = elevation.get_copernicus_tile_urls(bbox_NW, url_base="")
    expected_NW = [
        "Copernicus_DSM_COG_10_N40_00_W090_00_DEM/Copernicus_DSM_COG_10_N40_00_W090_00_DEM.tif"
    ]
    assert actual_NW == expected_NW


def test_get_copernicus_tile_url_single_tile_in_NE_hemisphere():
    bbox_NE = (89.1, 40.1, 89.9, 40.9)
    actual_NE = elevation.get_copernicus_tile_urls(bbox_NE, url_base="")
    expected_NE = [
        "Copernicus_DSM_COG_10_N40_00_E089_00_DEM/Copernicus_DSM_COG_10_N40_00_E089_00_DEM.tif"
    ]
    assert actual_NE == expected_NE


def test_get_copernicus_tile_url_single_tile_in_SW_hemisphere():
    bbox_SW = (-89.9, -40.1, -89.1, -40.9)
    actual_SW = elevation.get_copernicus_tile_urls(bbox_SW, url_base="")
    expected_SW = [
        "Copernicus_DSM_COG_10_S41_00_W090_00_DEM/Copernicus_DSM_COG_10_S41_00_W090_00_DEM.tif"
    ]
    assert actual_SW == expected_SW


def test_get_copernicus_tile_url_single_tile_in_SE_hemisphere():
    bbox_SE = (89.1, -40.1, 89.9, -40.9)
    actual_SE = elevation.get_copernicus_tile_urls(bbox_SE, url_base="")
    expected_SE = [
        "Copernicus_DSM_COG_10_S41_00_E089_00_DEM/Copernicus_DSM_COG_10_S41_00_E089_00_DEM.tif"
    ]
    assert actual_SE == expected_SE


def test_get_copernicus_tile_url_two_latitude_in_NE_hemisphere():
    bbox_NE = (89.1, 39.9, 89.9, 40.9)
    actual_two_lat = elevation.get_copernicus_tile_urls(bbox_NE, url_base="")
    expected_two_lat = [
        "Copernicus_DSM_COG_10_N39_00_E089_00_DEM/Copernicus_DSM_COG_10_N39_00_E089_00_DEM.tif",
        "Copernicus_DSM_COG_10_N40_00_E089_00_DEM/Copernicus_DSM_COG_10_N40_00_E089_00_DEM.tif",
    ]
    assert actual_two_lat == expected_two_lat


def test_get_copernicus_tile_url_two_longitude_in_NE_hemisphere():
    bbox_NE = (89.9, 40.1, 90.1, 40.9)
    actual_two_lon = elevation.get_copernicus_tile_urls(bbox_NE, url_base="")
    expected_two_lon = [
        "Copernicus_DSM_COG_10_N40_00_E089_00_DEM/Copernicus_DSM_COG_10_N40_00_E089_00_DEM.tif",
        "Copernicus_DSM_COG_10_N40_00_E090_00_DEM/Copernicus_DSM_COG_10_N40_00_E090_00_DEM.tif",
    ]
    assert actual_two_lon == expected_two_lon


def test_get_copernicus_tile_url_two_longitude_and_two_latitude_in_NE_hemisphere():
    bbox_NE = (89.9, 39.9, 90.1, 40.1)
    actual_two_lon_two_lat = elevation.get_copernicus_tile_urls(bbox_NE, url_base="")
    expected_two_lon_two_lat = [
        "Copernicus_DSM_COG_10_N40_00_E089_00_DEM/Copernicus_DSM_COG_10_N40_00_E089_00_DEM.tif",
        "Copernicus_DSM_COG_10_N39_00_E089_00_DEM/Copernicus_DSM_COG_10_N39_00_E089_00_DEM.tif",
        "Copernicus_DSM_COG_10_N40_00_E089_00_DEM/Copernicus_DSM_COG_10_N40_00_E089_00_DEM.tif",
        "Copernicus_DSM_COG_10_N40_00_E090_00_DEM/Copernicus_DSM_COG_10_N40_00_E090_00_DEM.tif",
    ]
    for expected in expected_two_lon_two_lat:
        assert expected in actual_two_lon_two_lat
    assert len(actual_two_lon_two_lat) == 4
