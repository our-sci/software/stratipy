import pytest
import numpy as np
import rasterio as rio
import shapely


@pytest.fixture
def basic_geometry():
    """
    Returns
    -------

    dict: GeoJSON-style geometry object.
        Coordinates are in grid coordinates (Affine.identity()).
    """

    return {
        "type": "Polygon",
        "coordinates": [[(2, 2), (2, 4.25), (4.25, 4.25), (4.25, 2), (2, 2)]],
    }


@pytest.fixture
def rotation_geometry():
    """
    Returns
    -------

    dict: GeoJSON-style geometry object.
        Coordinates are in grid coordinates (Affine.identity()).
    """

    return {
        "type": "Polygon",
        "coordinates": [
            [
                (481070, 4481140),
                (481040, 4481160),
                (481035, 4481130),
                (481060, 4481125),
                (481070, 4481140),
            ]
        ],
    }


@pytest.fixture
def geojson_multipolygon():
    """
    Returns
    -------

    dict: GeoJSON-style MultiPolygon geometry object.
        Coordinates are in grid coordinates (Affine.identity()).
    """

    return {
        "type": "MultiPolygon",
        "coordinates": (
            (((2, 2), (2, 4), (4, 4), (4, 2), (2, 2)),),
            (((0, 0), (0, 1), (1, 1), (1, 0), (0, 0)),),
        ),
    }


from shapely.geometry import shape, box, Polygon
from stratipy import utils
import utm
from math import floor
import pyproj


def test_get_bbox(rotation_geometry):
    geo = shape(rotation_geometry)
    expected_bbox = (481035.0, 4481125.0, 481070.0, 4481160.0)
    expected_padded_bbox = (
        expected_bbox[0] - 1,
        expected_bbox[1] - 1,
        expected_bbox[2] + 1,
        expected_bbox[3] + 1,
    )
    assert utils.get_bbox(geo) == expected_bbox
    assert utils.get_bbox(geo, padding=1) == expected_padded_bbox


def test_reproject_shape(basic_geometry):
    geo = box(-90, 40, -89, 41)
    # geo_utm = Polygon(
    #     [utm.from_latlon(lat, lon)[0:2] for lon, lat in geo.exterior.coords]
    # )
    reprojected = utils.reproject_shape(geo, "EPSG:4326", "EPSG:32616")
    actual = [floor(x * 100) / 100 for x in reprojected.bounds]
    expected = [
        floor(x * 100) / 100
        for x in (
            243900.35202972306,
            4429672.973115527,
            331792.11480619386,
            4543092.954431227,
        )
    ]
    assert actual == expected


# def test_bbox_to_utm(bbox):
#     # expected = []
#     utils.bbox_to_utm()

# def test_transform_array_to_affine():

# @pytest.mark.parametrize(
#     'raster,transform,shapes,expected',
#     [
#         pytest.param(
#             # the raster is positioned between x in (5, 8) and y in (-14, -10)
#             np.ones((4, 3), dtype=np.float32),
#             rio.transform.Affine(1, 0, 5, 0, -1, -10),
#             # mask out two pixels
#             # box params: xmin, ymin, xmax, ymax
#             [shapely.geometry.box(6.1, -12.9, 6.9, -11.1)],
#             np.array([
#                 [0, 0, 0],
#                 [0, 1, 0],
#                 [0, 1, 0],
#                 [0, 0, 0],
#             ], dtype=np.float32),
#         ),
#     ],
# )
# def test_mask_raster_with_geometry(raster, transform, shapes, expected):
#     np.testing.assert_array_equal(
#         utils.mask_raster_with_geometry(raster, transform, shapes),
#         expected
#     )
