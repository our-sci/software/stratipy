import pytest
from stratipy import sentinel2
import re


@pytest.mark.vcr()
def test_search_catalog_with_default_dates():
    bbox = [-73.11, 43.99, -73.12, 44.05]
    actual = sentinel2.search_catalog(bbox)
    assert len(actual["features"]) == 146


@pytest.mark.vcr
def test_search_catalog_with_custom_dates():
    bbox = [-73.11, 43.99, -73.12, 44.05]
    dates = "2020-01-01/2020-01-31"
    actual = sentinel2.search_catalog(bbox, dates)
    assert len(actual["features"]) == 13


@pytest.mark.vcr
def test_search_catalog_with_custom_query():
    search_query = {
        "eo:cloud_cover": {"lt": 50},
        "sentinel:valid_cloud_cover": {"eq": True},
    }
    bbox = [-73.11, 43.99, -73.12, 44.05]
    actual = sentinel2.search_catalog(bbox, query=search_query)
    assert len(actual["features"]) == 44


def test_search_catalog_with_invalid_query():
    bbox = [-73.11, 43.99, -73.12, 44.05]
    with pytest.raises(ValueError):
        sentinel2.search_catalog(bbox, query=[])

    with pytest.raises(ValueError):
        sentinel2.search_catalog(bbox, query={"a": []})


def test_search_catalog_without_bbox():
    with pytest.raises(ValueError):
        sentinel2.search_catalog([])


@pytest.mark.usefixtures("sentinel2_search_2_results")
def test_get_inputs_with_default_bands(sentinel2_search_2_results):
    actual = sentinel2.get_inputs(sentinel2_search_2_results)
    assert set(actual.keys()) == set(
        ["S2A_18TXP_20200131_0_L2A", "S2A_18TXP_20200128_0_L2A"]
    )
    expected = {
        "S2A_18TXP_20200131_0_L2A": {
            "B04": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200131_0_L2A/B04.tif",
            "B08": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200131_0_L2A/B08.tif",
            "SCL": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200131_0_L2A/SCL.tif",
        },
        "S2A_18TXP_20200128_0_L2A": {
            "B04": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200128_0_L2A/B04.tif",
            "B08": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200128_0_L2A/B08.tif",
            "SCL": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200128_0_L2A/SCL.tif",
        },
    }
    assert actual == expected


@pytest.mark.usefixtures("sentinel2_search_2_results")
def test_get_inputs_with_other_bands(sentinel2_search_2_results):
    actual = sentinel2.get_inputs(
        sentinel2_search_2_results, bands=["B01", "B02", "B03"]
    )
    expected = {
        "S2A_18TXP_20200131_0_L2A": {
            "B01": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200131_0_L2A/B01.tif",
            "B02": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200131_0_L2A/B02.tif",
            "B03": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200131_0_L2A/B03.tif",
        },
        "S2A_18TXP_20200128_0_L2A": {
            "B01": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200128_0_L2A/B01.tif",
            "B02": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200128_0_L2A/B02.tif",
            "B03": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/18/T/XP/2020/1/S2A_18TXP_20200128_0_L2A/B03.tif",
        },
    }
    assert actual == expected


@pytest.mark.usefixtures("sentinel2_search_2_results")
def test_get_inputs_with_invalid_bands(sentinel2_search_2_results):
    with pytest.raises(ValueError):
        sentinel2.get_inputs(sentinel2_search_2_results, bands=["AA0", "B02", "B03"])


@pytest.mark.usefixtures("sentinel2_search_2_results")
def test_get_inputs_with_no_bands(sentinel2_search_2_results):
    with pytest.raises(ValueError):
        sentinel2.get_inputs(sentinel2_search_2_results, bands=[])


def test_get_inputs_with_filter():
    results = {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "id": "S2A_18TXP_20200131_0_L2A",
                "assets": {
                    "B01": {
                        "href": "https://example.com/B01.tif",
                    },
                },
            },
            {
                "type": "Feature",
                "id": "S2B_18TXP_20200131_0_L2A",
                "assets": {
                    "B01": {
                        "href": "https://example.com/B01.tif",
                    },
                },
            },
        ],
    }
    # filter out S2B items
    filt = lambda item: re.match(r"^S2A.*$", item["id"])
    actual = sentinel2.get_inputs(results, bands=["B01"], filt=filt)
    assert len(actual.keys()) == 1
    assert "S2A_18TXP_20200131_0_L2A" in actual


@pytest.mark.usefixtures("sentinel2_search_2_results")
def test_get_inputs_with_invalid_filter(sentinel2_search_2_results):
    with pytest.raises(ValueError):
        sentinel2.get_inputs(sentinel2_search_2_results, filt="S2A")
