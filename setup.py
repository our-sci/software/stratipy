import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

__version__ = "0.2.1"

setuptools.setup(
    name="stratipy",
    version=__version__,
    author="Will Gardiner, Daniel Kane, Joel McClure",
    author_email="gardiner.w@gmail.com",
    license="GPLv3",
    description="Stratify boundaries for sampling",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/our-sci/software/stratipy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Scientific/Engineering",
    ],
    python_requires=">=3.7",
    install_requires=[
        "Shapely==1.8",
        "utm==0.7.0",
        "affine==2.3.1",
        "rasterio==1.3.6",
        "Fiona==1.8.21",
        "geopandas==0.10.2",
        "landlab==2.6.0",
        "tqdm==4.64.0",
        "pyproj>=3.2.1",
        "aiohttp==3.8.1",
        "pandas~=1.5.3",
        "numpy>=1.21.6",
        "pystac-client==0.3.2",
        "xmltodict~=0.13.0",
    ],
)
