# Changelog

<!--next-version-placeholder-->

## v0.2.1 (2023-05-22)
### Fix
* Update landlab to remove richdem dependency, which was breaking install ([`cb74124`](https://gitlab.com/our-sci/software/stratipy/-/commit/cb7412416ff74de9d7a108a44652c47335bd67bf))

## v0.2.0 (2023-05-20)
### Feature
* Update io to include sentinel 2 search query parameter ([`1606ae8`](https://gitlab.com/our-sci/software/stratipy/-/commit/1606ae8e96d7361e62fd3903751c0be70bffdffd))

## v0.1.0 (2023-05-18)
### Feature
* Add sampling presets for mhc ([`2c16d9d`](https://gitlab.com/our-sci/software/stratipy/-/commit/2c16d9df7b7f6982db55bef09c9329232ebeed3c))

## v0.0.1 (2023-05-18)
### Fix
* Set version to 0.0.0 to appease semantic-release ([`c4757cd`](https://gitlab.com/our-sci/software/stratipy/-/commit/c4757cd8a963ff72427c62c4adf9aa757ab8e147))
* Fix branch for sematic release ([`3ffd08b`](https://gitlab.com/our-sci/software/stratipy/-/commit/3ffd08b056f2c0d605b983ab71d98fa1185ce173))

## v0.8.2 (2022-06-21)
### Fix
* Fix gitlab-ci.yml ([`321fda5`](https://gitlab.com/our-sci/software/stratipy/-/commit/321fda534ab5eda9d39f8ae6529d724ce2cd4bd1))

## v0.8.1 (2022-05-17)
### Fix
* Clean up gitlab-ci ([`52b03bf`](https://gitlab.com/our-sci/software/stratipy/-/commit/52b03bfd4ece72d27d5328fbe8eed1fd14d8db08))
