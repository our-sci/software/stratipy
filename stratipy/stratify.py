from math import ceil
from numpy import clip


def get_sampling_density(stratum_area, densities):
    """
    calculate sampling density with stratipy presents and stratum area

        Parameters:
            stratum_area: array, required
                field to be stratified
            densities: tuple, required
                sampling density from stratipy presets

    """

    for density in densities:
        if stratum_area > density["min_area"] and stratum_area <= density["max_area"]:
            return density["density"]

    raise ValueError("invalid stratum area")


def get_number_of_samples(stratum_area, densities):
    """
    calculate number of samples for CLHS with stratipy presents and stratum area

    Parameters:
        stratum_area: array, required
            field to be stratified
        densities: tuple, required
            sampling density from stratipy presets

    """

    for density in densities:
        if stratum_area > density["min_area"] and stratum_area <= density["max_area"]:
            sampling_min = density.get("min_samplings", 1)
            sampling_max = density.get("max_samplings", float("inf"))
            num_samples = ceil(stratum_area * density["density"])
            return int(clip(num_samples, sampling_min, sampling_max))

    raise ValueError("invalid stratum area")
