from bson.objectid import ObjectId
import shapely
import datetime
import json
from stratipy.constants import (
    INPUT_LAYER_SENTINEL_2_L2A,
    INPUT_LAYER_3DEP,
    INPUT_LAYER_SSURGO,
    INPUT_LAYER_COPERNICUS_GLO_30,
)


def load_fields_from_projects_file(path):
    """
    Opens a project file and returns the fields

    Parameters:
        path: string, required
            a path to the desired file to load. The specified
            file should be JSON containing an array of projects
            e.g. projects = [
                {
                    producers: [
                        {
                            fields: [
                                {
                                    "boundaryGeoJSON": {
                                        # ...
                                    }
                                    # ...
                                }
                            ]
                        }
                    ]
                }
            ]
    """
    with open(path, "r") as f:
        projects = json.load(f)

    fields = []
    for project in projects:
        for producer in project["producers"]:
            fields.extend(producer["fields"])

    return fields


def fields_dict_by_human_id(fields):
    return {field["fieldByProjectId"]: field for field in fields}


def create_field_v1(
    areaIds,
    xid=None,
    name="Field",
    contactPoint=[],
):
    if xid is None:
        xid = str(ObjectId())

    return {
        "address": {},
        "contactPoint": contactPoint,
        "area": areaIds,
        "name": name,
        "id": xid,
    }


def create_field(
    areaIds,
    xid=None,
    name="Field",
    contactPoint=[],
):
    if xid is None:
        xid = str(ObjectId())

    return {
        "address": {},
        "contactPoint": contactPoint,
        "areas": areaIds,
        "name": name,
        "id": xid,
    }


def create_area(
    boundary,
    xid=None,
    name="Field Boundary",
    field_id=None,
    creator=None,
):
    if not shapely.geometry.shape(boundary).is_valid or (
        boundary["type"] not in ["Polygon", "MultiPolygon"]
    ):
        raise ValueError("invalid boundary")

    if xid is None:
        xid = str(ObjectId())

    return {
        "id": xid,
        "geometry": boundary,
        "properties": {
            "name": name,
            "featureOfInterest": field_id,
            "dateCreated": f"{datetime.datetime.utcnow().replace(microsecond=0).isoformat()}Z",
            "creator": creator,
        },
        "type": "Feature",
    }


def create_stratification(
    area_id=None,
    xid=None,
    name="Stratification",
    algorithm_version=None,
    agent=None,
    random_seed=None,
    clhs_max_iterations=1000,
    number_of_clusters=None,
    sample_density=None,
    boundary_buffer_meters=None,
    clhs_buffer_meters=None,
    s2_masked_ratio_threshold=None,
    s2_ndvi_max=1.0,
    s2_ndvi_date_range="2021-01-01/2021-12-31",
    s2_search_query={"platform": {"eq": "sentinel-2a"}},
    input_layers=[INPUT_LAYER_SENTINEL_2_L2A, INPUT_LAYER_3DEP, INPUT_LAYER_SSURGO],
):
    if xid is None:
        xid = str(ObjectId())

    input_layers_dict = {
        INPUT_LAYER_SENTINEL_2_L2A: [
            {
                "name": INPUT_LAYER_SENTINEL_2_L2A,
                "value": "https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi/product-types/level-2a",
            },
            {
                "name": "INPUT_LAYER_SENTINEL_2_L2A_SCL_MASKED_RATIO_THRESHOLD",
                "value": s2_masked_ratio_threshold,
            },
            {
                "name": "INPUT_LAYER_SENTINEL_2_L2A_NDVI_MAX_VALUE",
                "value": s2_ndvi_max,
            },
            {
                "name": "INPUT_LAYER_SENTINEL_2_L2A_NDVI_DATE_RANGE",
                "value": s2_ndvi_date_range,
            },
            {
                "name": "INPUT_LAYER_SENTINEL_2_L2A_SEARCH_QUERY",
                "value": s2_search_query,
            },
        ],
        INPUT_LAYER_3DEP: [
            {
                "name": INPUT_LAYER_3DEP,
                "value": "https://www.usgs.gov/core-science-systems/ngp/3dep",
            },
        ],
        INPUT_LAYER_SSURGO: [
            {
                "name": INPUT_LAYER_SSURGO,
                "value": "https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/survey/?cid=nrcs142p2_053627",
            },
        ],
        INPUT_LAYER_COPERNICUS_GLO_30: [
            {
                "name": INPUT_LAYER_COPERNICUS_GLO_30,
                "value": "https://portal.opentopography.org/datasetMetadata?otCollectionID=OT.032021.4326.1",
            },
        ],
    }

    for layer in input_layers:
        if layer not in input_layers_dict:
            raise ValueError(f"layer {layer} is unknown")

    return {
        "id": xid,
        "object": area_id,
        "name": name,
        "agent": agent,
        "dateCreated": f"{datetime.datetime.utcnow().replace(microsecond=0).isoformat()}Z",
        "algorithm": {
            "name": "OUR_SCI_STRATIFICATION",
            "alternateName": "SKLEARN_KMEANS_RANGE_CLHS",
            # codeRepository?: string;
            "version": algorithm_version,
            # doi?: string;
        },
        "provider": "OUR_SCI",
        "input": [
            *(
                input_layers_dict["INPUT_LAYER_SENTINEL_2_L2A"]
                if "INPUT_LAYER_SENTINEL_2_L2A" in input_layers
                else []
            ),
            *(
                input_layers_dict["INPUT_LAYER_3DEP"]
                if "INPUT_LAYER_3DEP" in input_layers
                else []
            ),
            *(
                input_layers_dict["INPUT_LAYER_SSURGO"]
                if "INPUT_LAYER_SSURGO" in input_layers
                else []
            ),
            *(
                input_layers_dict["INPUT_LAYER_COPERNICUS_GLO_30"]
                if "INPUT_LAYER_COPERNICUS_GLO_30" in input_layers
                else []
            ),
            {
                "name": "RANDOM_SEED",
                "value": random_seed,
            },
            {
                "name": "CLHS_MAX_ITERATIONS",
                "value": clhs_max_iterations,
            },
            {
                "name": "CLUSTERING_NUMBER_OF_CLUSTERS",
                "value": number_of_clusters,
            },
            {
                "name": "SAMPLE_DENSITY",
                "value": sample_density,
            },
            {
                "name": "FIELD_BOUNDARY_RASTER_MASK_BUFFER_METERS",
                "value": boundary_buffer_meters,
            },
            *(
                [
                    {
                        "name": "BOUNDARY_CLHS_RASTER_MASK_BUFFER_METERS",
                        "value": clhs_buffer_meters,
                    }
                ]
                if clhs_buffer_meters is not None
                else []
            ),
        ],
        "result": [],
    }


def create_location_collection(
    xid=None,
    stratification_id=None,
    area_id=None,
    field_id=None,
    locations=[],
):
    if xid is None:
        xid = str(ObjectId())

    return {
        "id": xid,
        "resultOf": stratification_id,
        "featureOfInterest": field_id,
        "object": area_id,
        "features": locations,
    }


def create_location(
    point_coords,
    xid=None,
    stratum: str = None,
    label: str = None,
    composite: str = None,
    bulk_density_required: bool = None,
):
    if xid is None:
        xid = str(ObjectId())

    return {
        "id": xid,
        "type": "Feature",
        "properties": {
            **({"stratum": str(stratum)} if stratum is not None else {}),
            **({"label": str(label)} if label is not None else {}),
            **({"composite": str(composite)} if composite is not None else {}),
            **(
                {"bulkDensityRequired": bulk_density_required}
                if bulk_density_required is not None
                else {}
            ),
        },
        "geometry": {
            "type": "Point",
            "coordinates": point_coords,
        },
    }


def create_export_file(
    location_coords,
    boundary,
    name=None,
    stratification_args={},
    location_strata=[],
):
    if name is None:
        name = (
            f"{datetime.datetime.utcnow().replace(microsecond=0).isoformat()}Z".replace(
                ":", "_"
            )
        )

    if len(location_strata) > 0 and len(location_strata) != len(location_coords):
        raise ValueError("location_strata does not match location_coords")

    if len(location_strata) > 0:
        locations = [
            create_location(c, stratum=s)
            for c, s in zip(location_coords, location_strata)
        ]
    else:
        locations = [create_location(c, xid=str(ObjectId())) for c in location_coords]

    area = create_area(boundary, name=f"Field {name} Area")
    field = create_field_v1([area["id"]], name=f"Field {name}")
    stratification = create_stratification(
        area_id=area["id"],
        name=f"Field {name} Stratification",
        **stratification_args,
    )
    location_collection = create_location_collection(
        stratification_id=stratification["id"],
        field_id=field["id"],
        area_id=area["id"],
        locations=[loc["id"] for loc in locations],
    )
    return json.dumps(
        {
            "areas": [area],
            "fields": [field],
            "stratifications": [stratification],
            "locations": locations,
            "locationCollections": [location_collection],
        }
    )


def create_stratification_export(
    location_coords,
    area_id=None,
    field_id=None,
    name=None,
    stratification_args={},
    location_strata=[],
):
    """
    Creates a json string to POST to the SoilStack Soil API to create a new
    Stratification with accompanying LocationCollection and Locations
    """
    if name is None:
        name = (
            f"{datetime.datetime.utcnow().replace(microsecond=0).isoformat()}Z".replace(
                ":", "_"
            )
        )

    if area_id is None:
        raise ValueError("area id is required")

    if field_id is None:
        raise ValueError("field id is required")

    if len(location_strata) > 0 and len(location_strata) != len(location_coords):
        raise ValueError("location_strata does not match location_coords")

    if len(location_strata) > 0:
        locations = [
            create_location(c, stratum=str(s), label=str(i))
            for i, (c, s) in enumerate(zip(location_coords, location_strata))
        ]
    else:
        locations = [
            create_location(c, xid=str(ObjectId()), label=str(i))
            for i, c in enumerate(location_coords)
        ]

    stratification = create_stratification(
        area_id=area_id,
        name=f"Field {name} Stratification",
        **stratification_args,
    )
    location_collection = create_location_collection(
        stratification_id=stratification["id"],
        field_id=field_id,
        area_id=area_id,
        locations=[loc["id"] for loc in locations],
    )

    return json.dumps(
        {
            "stratification": {
                k: v for k, v in stratification.items() if k not in ["id"]
            },
            "locationCollection": {
                **{
                    k: v
                    for k, v in location_collection.items()
                    if k not in ["id", "resultOf"]
                },
                "features": [
                    {k: v for k, v in loc.items() if k not in ["id"]}
                    for loc in locations
                ],
            },
        }
    )
