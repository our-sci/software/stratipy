from math import floor, ceil
import itertools
from landlab import RasterModelGrid
import rasterio as rio
from numpy import zeros
from stratipy.fetch import fetch_raster_async
from stratipy.utils import merge_rasters
from typing import Tuple, List
import warnings
import aiohttp
import asyncio
import xmltodict


def bbox_product_range(bbox):
    """Generate list of coordinate pairs covered by the BBOX in degrees
    Covers all of the world can handle negitive and positive longitudes and latitutes

    Parameters:
    bbox: tuple, required
        bounding box (minx, miny, maxx, maxy) in WGS84 CRS
    """

    minX, minY, maxX, maxY = bbox

    x_vals = range(floor(minX), floor(maxX + 1))
    y_vals = range(floor(minY), floor(maxY + 1))

    return list(itertools.product(x_vals, y_vals))


def get_3dep_tile_urls(
    bbox: Tuple[float],
    url_base: str = "https://prd-tnm.s3.amazonaws.com/StagedProducts/Elevation/13/TIFF/current/",
) -> List[str]:
    """Generate 3DEP dataset tile urls from long/lat bounding box
    Only covers US, so it only covers negative longitudes and positive latitudes.

    Parameters:
        bbox: tuple, required
            bounding box (minx, miny, maxx, maxy) in NAD83 CRS
        url_base: string, re

    See reference: https://www.sciencebase.gov/catalog/item/5f7784f982ce1d74e7d6cbd5
    """
    minX, minY, maxX, maxY = bbox
    tiles = [
        f"n{lat}{f'w{str(lon)[1:].zfill(3)}' if lon < 0 else f'e{lon:03}'}"
        for (lon, lat) in itertools.product(
            range(floor(minX), ceil(maxX)), range(floor(minY) + 1, ceil(maxY) + 1)
        )
    ]
    return list(map(lambda x: f"{url_base}{x}/USGS_13_{x}.tif", tiles))


def get_3dep_tiles(
    bbox: Tuple[float],
    url_base: str = "https://prd-tnm.s3.amazonaws.com/StagedProducts/Elevation/13/TIFF/current/",
) -> List[str]:
    """Generate 3DEP dataset tile and metadata urls from long/lat bounding box
    Only covers US, so it only covers negative longitudes and positive latitudes.

    Parameters:
        bbox: tuple, required
            bounding box (minx, miny, maxx, maxy) in NAD83 CRS
        url_base: string, re

    See reference: https://www.sciencebase.gov/catalog/item/5f7784f982ce1d74e7d6cbd5
    """
    minX, minY, maxX, maxY = bbox
    coord_pairs = itertools.product(
        range(floor(minX), ceil(maxX)), range(floor(minY) + 1, ceil(maxY) + 1)
    )

    pair_to_name = (
        lambda lon, lat: f"n{lat}{f'w{str(lon)[1:].zfill(3)}' if lon < 0 else f'e{lon:03}'}"
    )
    name_to_url = lambda name, ext: f"{url_base}{name}/USGS_13_{name}.{ext}"

    tiles = {}
    for (lon, lat) in coord_pairs:
        name = pair_to_name(lon, lat)
        tiles[name] = {
            "url": name_to_url(name, "tif"),
            "metadata": name_to_url(name, "xml"),
        }

    return tiles


def get_copernicus_tile_urls(
    bbox: Tuple[float, float, float, float],
    url_base: str = "https://copernicus-dem-30m.s3.amazonaws.com/",
    resolution_arc_sec: str = "10",
) -> List[str]:
    """Generate Copernicus DEM dataset tile urls from long/lat bounding box

    Parameters:
        bbox: tuple, required
            bounding box (minx, miny, maxx, maxy) in WSG84 CRS
        url_base: string
        resolution_arc_sec: string,
            resolution in arc seconds 10 or 30 for 30m and 90m resolution respectivly

    s3://copernicus-dem-90m/Copernicus_DSM_COG_30_S90_00_W178_00_DEM/
    Copernicus_DSM_COG_[resolution][northing][easting]_DEM/

    See reference: https://copernicus-dem-30m.s3.amazonaws.com/readme.html

    In original files, this is the northing of the center of the bottom-most pixels,
    while in our files, because we removed the bottom-most pixels, the center of the
    new bottom-most pixels is one pixel-length (resolution) away to north. [easting]
    = e.g. w125_00 - decimal degrees where the decimal part is always 00.
    """

    tiles_urls = []
    tiles = bbox_product_range(bbox)

    for lon, lat in tiles:

        lon_sign = "E" if lon > 0 else "W"
        lat_sign = "N" if lat > 0 else "S"

        image_file_name = f"Copernicus_DSM_COG_{resolution_arc_sec}_{lat_sign}{str(abs(lat)).zfill(2)}_00_{lon_sign}{str(abs(lon)).zfill(3)}_00_DEM"
        tiles_urls.append(f"{url_base}{image_file_name}/{image_file_name}.tif")

    return tiles_urls


async def fetch_tile_metadata(tiles):
    results = {}
    async with aiohttp.ClientSession() as session:
        for tile_key in tiles:
            async with session.get(tiles[tile_key]["metadata"]) as resp:
                payload = await resp.text()
                results[tile_key] = xmltodict.parse(payload)

    return {k: {"url": v["url"], "metadata": results[k]} for k, v in tiles.items()}


def metadata_to_bbox(meta):
    bounds = meta["metadata"]["idinfo"]["spdom"]["bounding"]
    return [
        float(bounds["westbc"]),
        float(bounds["southbc"]),
        float(bounds["eastbc"]),
        float(bounds["northbc"]),
    ]


async def fetch_elevation(
    bbox,
    dst_crs=None,
    dst_transform=None,
    dst_shape=None,
    elevation_tile_urls_getter=get_3dep_tile_urls,
    resampling_algorithm="nearest",
):
    """
    fetch elevation data from supported source with the option to transform it to
    desired projection

    Parameters:
        bbox: tuple, required
            bounding box (minx, miny, maxx, maxy) in WSG84 CRS
        dst_crs: string, optional
            target coordinate reference system
        dst_transform: array, optional
            target affine transformation
        dst_shape: array, optional
            shape of target array
        elevation_tile_urls_getter: function, optional
            fuction that provides urls. One of: get_3dep_tile_urls,
            get_copernicus_tile_urls
        resampling_algorithm: string, optional
            algorithm to preform resampling. Options are from rio.warp.Resampling found
            at: https://rasterio.readthedocs.io/en/latest/topics/resampling.html
    """

    tile_urls = elevation_tile_urls_getter(bbox)
    results = await asyncio.gather(
        *[fetch_raster_async(url, bbox=bbox) for url in tile_urls],
        return_exceptions=True,
    )
    good_results = [x for x in results if type(x) == tuple]
    exceptions = [x for x in results if isinstance(x, Exception)]
    if len(exceptions) > 0:
        raise exceptions[0]

    if len(set(ds[2] for ds in good_results)) > 1:
        raise ValueError("All rasters must be in the same CRS.")

    if any(
        elem is not None for elem in [dst_crs, dst_transform, dst_shape]
    ) and not all(elem is not None for elem in [dst_crs, dst_transform, dst_shape]):
        warnings.warn(
            "dst_crs, dst_transform, and dst_shape are all required for reprojection"
        )

    merged, merged_tf, merged_crs = merge_rasters(good_results)

    valid_resampling_modes = list(rio.warp.Resampling.__members__.keys())
    if resampling_algorithm not in valid_resampling_modes:
        raise ValueError(
            f"Invalid resampling mode. Value must be one of {valid_resampling_modes}"
        )

    if dst_crs is not None and dst_transform is not None:
        with rio.Env():
            ele_reproj, ele_reproj_tf = rio.warp.reproject(
                merged,
                destination=zeros(dst_shape),
                src_transform=merged_tf,
                src_crs=merged_crs,
                dst_transform=dst_transform,
                dst_crs=dst_crs,
                resampling=rio.warp.Resampling[resampling_algorithm],
            )
        return ele_reproj, ele_reproj_tf, dst_crs

    return merged, merged_tf, merged_crs


def calculate_slope_aspect(elev):
    mg = RasterModelGrid(elev.shape)
    z = elev.flatten()
    slope = mg.calc_slope_at_node(elevs=z).reshape(elev.shape)
    aspect = mg.calc_aspect_at_node(elevs=z).reshape(elev.shape)
    return slope, aspect
