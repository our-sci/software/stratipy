# Stratify

Stratify boundaries for sampling

## Installation
- create a python virtual environment

```
# tested with python 3.7 - 3.9, use a version management tool to install a compatible python version if necessary
# create virtual environment
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pre-commit install
```
